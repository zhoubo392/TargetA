//
//  ViewController.swift
//  TargetA
//
//  Created by zhoubo on 2018/3/12.
//  Copyright © 2018年 zhoubo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    public let l2 = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let l = UILabel()
        l.text = "i am taget A"
        self.view.addSubview(l)
        l.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        
        self.view.addSubview(l2)
        l2.frame = CGRect(x: 100, y: 300, width: 100, height: 100)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
