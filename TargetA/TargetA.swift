//
//  TargetA.swift
//  TargetA
//
//  Created by zhoubo on 2018/3/12.
//  Copyright © 2018年 zhoubo. All rights reserved.
//

import UIKit

public final class Target_A: NSObject {
    
    @objc public func Action_nativeViewController(_ params: Dictionary<String, AnyObject>) -> UIViewController {
        let viewController = ViewController()
        viewController.l2.text = params["key"] as? String
        return viewController
    }
}
